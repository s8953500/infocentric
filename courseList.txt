Programming in C
Pre-requisites: 
Distributed Computing
Pre-requisites: Programming in C, Advanced Programming in Java, Scalable Machine Learning
Database System
Pre-requisites: Programming in C, Programming in Java, Advanced Programming in Java, Big Data with Apache Spark, Data Structures
Algorithm 1
Pre-requisites: Programming in C, Programming in Perl, Database System
Algorithm 2
Pre-requisites: Programming in C, Algorithm 1, Database System, Data Structures
Programming in Java
Pre-requisites: Programming in C
Advanced Programming in Java
Pre-requisites: Programming in Java
Big Data with Apache Spark
Pre-requisites: Programming in Java, Advanced Programming in Java, Probability, Data Structures
Programming in Perl
Pre-requisites: Algorithm 2
Probability
Pre-requisites: 
Scalable Machine Learning
Pre-requisites: Probability, Big Data with Apache Spark
Data Structures
Pre-requisites: 
